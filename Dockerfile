FROM python:2-jessie

WORKDIR /app
COPY ./web/requirements.txt .
RUN pip install --no-cache-dir -r ./requirements.txt

# fixing SSL error in python library 
RUN apt-get update && apt-get install -y sed && apt-get clean
RUN sed -i 's/\(def get_server_certificate.*\)PROTOCOL_SSLv3\(.*\)/\1PROTOCOL_SSLv23\2/g' /usr/local/lib/python2.7/site-packages/gevent/ssl.py
COPY ./web/. .

CMD [ "python", "./application.py" ]
